﻿namespace CrocLocalisationTool.Services;

/// <summary>
/// Class to create- diff between loc files.
/// </summary>
internal class DiffService
{
    public IEnumerable<string> GetDiff(List<string> source, List<string> target)
    {
        return source.Except(target);
    }
}
