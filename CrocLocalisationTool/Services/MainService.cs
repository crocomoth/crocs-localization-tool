﻿using CrocLocalisationTool.Interfaces;
using CrocLocalisationTool.Models;

namespace CrocLocalisationTool.Services;

internal class MainService
{
    private readonly DiffService _diffService;
    private readonly Processor _processor;

    public MainService()
    {
        _diffService = new DiffService();
        _processor = new Processor();
    }

    public void ProcessFiles(string path, IWriter writer, List<string> filters)
    {
        var folders = Directory.GetDirectories(path).ToList();
        var dict = new Dictionary<string, List<string>>();

        // get files in folders
        foreach (var folder in folders)
        {
            if (filters.Any())
            {
                if (filters.Contains(Path.GetFileName(folder)))
                {
                    dict.Add(Path.GetFileName(folder), Directory.GetFiles(folder).ToList());
                }
            }
            else
            {
                dict.Add(Path.GetFileName(folder), Directory.GetFiles(folder).ToList());
            }
        }

        var languages = new List<Language>();

        // sort them into objects
        foreach (var languageFiles in dict)
        {
            var localisations = new List<Localisation>();

            foreach (var file in languageFiles.Value)
            {
                // we need only keys, without trailing spaces, not commented out lines and not system lines
                var loc = File.ReadAllLines(file)
                    .Select(s => s.Trim())
                    .Where(s => !string.IsNullOrWhiteSpace(s) && !s.StartsWith("l_") && !s.StartsWith("#"))
                    .Select(s => _processor.GetLocKey(s)).ToList();

                // need to replace language name to make a match between different languages
                localisations.Add(new Localisation(Path.GetFileNameWithoutExtension(file).Replace(languageFiles.Key, ""), loc));
            }

            languages.Add(new Language(languageFiles.Key, localisations));
        }

        ProcessDiff(writer, languages);
    }

    public void ProcessAsSingle(string path, IWriter writer, List<string> filters)
    {
        var folders = Directory.GetDirectories(path).ToList();
        var dict = new Dictionary<string, List<string>>();

        // get files in folders
        foreach (var folder in folders)
        {
            if (filters.Any())
            {
                if (filters.Contains(Path.GetFileName(folder)))
                {
                    dict.Add(Path.GetFileName(folder), Directory.GetFiles(folder).ToList());
                }
            }
            else
            {
                dict.Add(Path.GetFileName(folder), Directory.GetFiles(folder).ToList());
            }
        }

        var languages = new List<Language>();

        // sort them into objects
        foreach (var languageFiles in dict)
        {
            var localisations = new List<Localisation>();

            var buf = new List<string>();

            foreach (var file in languageFiles.Value)
            {
                // we need only keys, without trailing spaces, not commented out lines and not system lines
                var loc = File.ReadAllLines(file)
                    .Select(s => s.Trim())
                    .Where(s => !string.IsNullOrWhiteSpace(s) && !s.StartsWith("l_") && !s.StartsWith("#"))
                    .Select(s => _processor.GetLocKey(s)).ToList();

                buf.AddRange(loc);
            }

            // we only care about language here, not files
            localisations.Add(new Localisation(Path.GetFileNameWithoutExtension(languageFiles.Key), buf));
            languages.Add(new Language(languageFiles.Key, localisations));
        }

        ProcessDiffAbsolute(writer, languages);
    }

    private void ProcessDiffAbsolute(IWriter writer, List<Language> languages)
    {
        // just look at ProcessDiff, this one is almost the same, but has 1 less cycle
        for (int i = 0; i < languages.Count; i++)
        {
            for (int j = i + 1; j < languages.Count; j++)
            {
                var source = languages[i].Files.FirstOrDefault();
                if (source is null)
                {
                    writer.Write($"Warning language was not parsed correctly {languages[i].Name}");
                    continue;
                }

                var target = languages[j].Files.FirstOrDefault();
                if (target is null)
                {
                    writer.Write($"Warning language was not parsed correctly {languages[j].Name}");
                    continue;
                }

                Console.WriteLine($"Starting to process {languages[i].Name} and {languages[j].Name}");
                var diff = _diffService.GetDiff(source.Items, target.Items);
                if (diff.Any())
                {
                    writer.Write($"Diff between {languages[i].Name} and {languages[j].Name}:");
                    foreach (var item in diff)
                    {
                        writer.Write(item);
                    }
                }

                Console.WriteLine($"Starting to process {languages[j].Name} and {languages[i].Name}");
                var reverseDiff = _diffService.GetDiff(target.Items, source.Items);
                if (reverseDiff.Any())
                {
                    writer.Write($"Diff between {languages[j].Name} and {languages[i].Name}:");
                    foreach (var item in reverseDiff)
                    {
                        writer.Write(item);
                    }
                }
            }
        }
    }

    private void ProcessDiff(IWriter writer, List<Language> languages)
    {
        for (int i = 0; i < languages.Count; i++)
        {
            for (int j = i + 1; j < languages.Count; j++)
            {
                foreach (var source in languages[i].Files)
                {
                    // get file with same name from other language
                    var target = languages[j].Files.FirstOrDefault(x => x.FileName.Equals(source.FileName));
                    if (target is null)
                    {
                        writer.Write($"Warning file {source.FileName} exists in {languages[i].Name} but not in {languages[j].Name}");
                        continue;
                    }

                    // get diff between files
                    var diff = _diffService.GetDiff(source.Items, target.Items);
                    if (diff.Any())
                    {
                        writer.Write($"Diff between {source.FileName}{languages[i].Name} and {target.FileName}{languages[j].Name}:");
                        foreach (var item in diff)
                        {
                            writer.Write(item);
                        }
                    }

                    // now get reverse diff to no to extra cycles
                    var reverseDiff = _diffService.GetDiff(target.Items, source.Items);
                    if (reverseDiff.Any())
                    {
                        writer.Write($"Diff between {target.FileName}{languages[j].Name} and {source.FileName}{languages[i].Name}:");
                        foreach (var item in reverseDiff)
                        {
                            writer.Write(item);
                        }
                    }
                }
            }
        }
    }
}
