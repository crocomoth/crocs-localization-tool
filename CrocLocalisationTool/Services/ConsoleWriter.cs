﻿using CrocLocalisationTool.Interfaces;

namespace CrocLocalisationTool.Services
{
    /// <summary>
    /// Simple logger/writer to console. Usually not really useful.
    /// </summary>
    internal class ConsoleWriter : IWriter
    {
        public void Write(string text)
        {
            Console.WriteLine(text);
        }
    }
}
