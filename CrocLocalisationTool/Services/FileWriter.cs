﻿using CrocLocalisationTool.Interfaces;
using System.Text;

namespace CrocLocalisationTool.Services
{
    /// <summary>
    /// Class to handle output for project.
    /// </summary>
    internal class FileWriter : IWriter
    {
        private readonly string path = AppDomain.CurrentDomain.BaseDirectory + "output.txt";
        private readonly FileStream stream;

        public FileWriter()
        {
            stream = File.Create(path);
        }

        public void Write(string text)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(text + "\r\n");
            stream.Write(info, 0, info.Length);
        }

        public void Close()
        {
            stream.Close();
        }
    }
}
