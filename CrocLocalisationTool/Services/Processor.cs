﻿namespace CrocLocalisationTool.Services
{
    internal class Processor
    {
        public string GetLocKey(string content)
        {
            var parts = content.Split(':', StringSplitOptions.RemoveEmptyEntries);
            // I may have made a mistake here?
            var name = parts[0].Replace(":0", string.Empty).Trim();

            return name;
        }
    }
}
