﻿namespace CrocLocalisationTool.Models
{
    /// <summary>
    /// Represents language in hoi4 localisation - is set of files with loc strings.
    /// </summary>
    internal class Language
    {
        public Language(string name, List<Localisation> files)
        {
            Name = name;
            Files = files;
        }

        public string Name { get; init; }
        public List<Localisation> Files { get; init; }
    }
}
