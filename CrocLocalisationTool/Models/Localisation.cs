﻿namespace CrocLocalisationTool.Models
{
    /// <summary>
    /// Represents localisation file within some language.
    /// </summary>
    internal class Localisation
    {
        public Localisation(string fileName, List<string> items)
        {
            FileName = fileName;
            Items = items;
        }

        public string FileName { get; init; }
        public List<string> Items { get; init; }
    }
}
