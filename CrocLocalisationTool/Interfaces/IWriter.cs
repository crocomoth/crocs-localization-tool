﻿namespace CrocLocalisationTool.Interfaces
{
    internal interface IWriter
    {
        public void Write(string text);
    }
}
