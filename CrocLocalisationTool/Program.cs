﻿using CrocLocalisationTool.Services;
using System.Text;

Console.OutputEncoding = Encoding.UTF8;
Console.InputEncoding = Encoding.UTF8;

Console.WriteLine("Welcome to croc loc tool");

var service = new MainService();

Console.WriteLine("Enter path to loc folder");

string? path;

do
{
    path = Console.ReadLine();
} while (string.IsNullOrWhiteSpace(path));

var writer = new FileWriter();

Console.WriteLine("Enter processing mode: file-by-file as 'file' or general as 'abs'");
Console.WriteLine("You can enter languages to use using ';' as delimeter e.g. 'abs;english;russian'");
string? command;

List<string> filters = new();

do
{
    var commandString = Console.ReadLine();
    if (string.IsNullOrEmpty(commandString))
    {
        continue;
    }

    var parts = commandString.Split(';', StringSplitOptions.RemoveEmptyEntries);
    if (parts.Length > 1)
    {
        for (int i = 1; i < parts.Length; i++)
        {
            filters.Add(parts[i]);
        }
    }

    command = parts[0];
    if (command == "file" || command == "abs")
    {
        break;
    }
} while (true);

if (command == "file")
{
    service.ProcessFiles(path, writer, filters);
}
else
{
    service.ProcessAsSingle(path, writer, filters);
}

writer.Close();

Console.WriteLine("Finished processing");
Console.ReadKey();
